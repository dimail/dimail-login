#!/bin/sh

version=$(cd src || exit; dpkg-parsechangelog --show-field Version | sed -e 's/-.*$//')
package=$(cd src || exit; dpkg-parsechangelog --show-field Source)
full="${package}_${version}"

tar -czf "$full".orig.tar.gz --transform="s/^src/$full/" src
cd src || exit
dpkg-buildpackage 
